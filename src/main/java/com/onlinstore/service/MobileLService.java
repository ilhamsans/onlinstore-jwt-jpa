package com.onlinstore.service;

import com.onlinstore.dao.MlbbDAO;
import com.onlinstore.model.DAOMobileL;
import com.onlinstore.model.MobileLDTO;
import com.onlinstore.model.MobileLDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class MobileLService {
    @Autowired
    private MlbbDAO mlbbDAO;

    public DAOMobileL save(MobileLDTO ml) {

        DAOMobileL newProduct = new DAOMobileL();
        newProduct.setDiamond(ml.getDiamond());
        newProduct.setHarga(ml.getHarga());
        return mlbbDAO.save(newProduct);

    }

    public List<DAOMobileL> getProductDao() {

        DAOMobileL prdk = new DAOMobileL();
        prdk.getDiamond();
        prdk.getHarga();

        return (List<DAOMobileL>) mlbbDAO.findAll();
    }

    public Optional<DAOMobileL> findById(int id) {

        DAOMobileL prdk = new DAOMobileL();
        prdk.getDiamond();
        prdk.getHarga();

        return mlbbDAO.findById(id);
    }

    public void delete(int id) {
        mlbbDAO.deleteById(id);
    }
}
