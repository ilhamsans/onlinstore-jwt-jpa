package com.onlinstore.dao;

import com.onlinstore.model.DAOMobileL;
import org.springframework.data.repository.CrudRepository;

public interface MlbbDAO extends CrudRepository<DAOMobileL, Integer> {
}
