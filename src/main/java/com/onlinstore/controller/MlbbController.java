package com.onlinstore.controller;

import com.onlinstore.model.DAOMobileL;
import com.onlinstore.model.MobileLDTO;
import com.onlinstore.service.MobileLService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class MlbbController {

    public static final Logger logger = LoggerFactory.getLogger(MlbbController.class);

    private MobileLService mobileLService;

    // -------------------Create a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/ml/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody MobileLDTO ml) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", ml);

        mobileLService.save(ml);

        return new ResponseEntity<>(ml, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/ml/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOMobileL>> kabeh() throws Exception {
        List<DAOMobileL> ml = mobileLService.getProductDao();
//        if (products.isEmpty()) {
//            return new ResponseEntity<>(products, HttpStatus.NOT_FOUND);
//        }
        return new ResponseEntity<>(ml, HttpStatus.OK);
    }
//
//    @RequestMapping(value = "/ml/{id}", method = RequestMethod.PUT)
//    public ResponseEntity<?> updateProduk(@PathVariable("id") int id, @RequestBody DAOProduct product) throws Exception {
//        logger.info("Updating produk with id {}", id);
//
////        product = productDao.find(id);
////        product.setName(product.getName());
////        product.setCategoryId(product.getCategoryId());
////        product.setPrice(product.getPrice());
//        Optional<DAOProduct> crntPrdk = productDetailService.findById(id);
//
//        if (crntPrdk == null) {
//            logger.error("Unable to update. produk with id {} not found.", id);
//            return new ResponseEntity<>(("\"Unable to upate. produk with id " + id + "not found."), HttpStatus.NOT_FOUND);
//        }
//
//        product.setName(product.getName());
//        product.setPrice(product.getPrice());
//        product.setCategoryId(product.getCategoryId());
//
//        productDetailService.updateProduct(crntPrdk);
//        return new ResponseEntity<>(crntPrdk, HttpStatus.OK);
//    }

    @RequestMapping(value = "/ml/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") int id) throws Exception {
        logger.info("Fetching Product with id {}", id);
        Optional<DAOMobileL> ml = mobileLService.findById(id);
        if (ml == null) {
            logger.error("Product with id {} not found.", id);
            return new ResponseEntity<>(("Product with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ml, HttpStatus.OK);
    }

    @RequestMapping(value = "/ml/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") int id) throws Exception {
        logger.info("Fetching & Deleting Product with id {}", id);


        mobileLService.delete(id);
        return new ResponseEntity<DAOMobileL>(HttpStatus.NO_CONTENT);
    }
}
